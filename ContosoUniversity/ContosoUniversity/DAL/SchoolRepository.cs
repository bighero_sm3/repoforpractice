﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Objects;
using ContosoUniversity.DAL;

namespace ContosoUniversity.DAL
{
    public class SchoolRepository : IDisposable, ISchoolRepository
    {
        private SchoolEntities context = new SchoolEntities();
        private bool disposedValue = false;

        public SchoolRepository()
        {
            //Because a web application typically has short-lived object context instances,
            //queries often return data that doesn't need to be tracked, because the object context
            //that reads them will be disposed before any of the entities it reads are used again or updated.
            context.Departments.MergeOption = MergeOption.NoTracking;
            context.InstructorNames.MergeOption = MergeOption.NoTracking;
            context.OfficeAssignments.MergeOption = MergeOption.NoTracking;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposedValue = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void SaveChanges()
        {
            try
            {
                context.SaveChanges();
            }
            catch (OptimisticConcurrencyException ocex)
            {
                context.Refresh(RefreshMode.StoreWins, ocex.StateEntries[0].Entity);
                throw ocex;
            }
        }


        #region Department

        /// <summary>
        /// The GetDepartments method returns an IEnumerable object in order to ensure that
        /// - The returned collection is usable even after the repository object itself is disposed.
        /// - Can perform typical read-only list processing tasks such as foreach loops and LINQ queries.
        /// - Cannot add to or remove items in the collection, which might imply that such changes would be persisted to the database.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Department> GetDepartments()
        {
            return GetDepartments("");
        }

        public IEnumerable<Department> GetDepartments(string sortExpression)
        {
            return GetDepartmentsByName(sortExpression, "");
        }

        public IEnumerable<Department> GetDepartmentsByName(string sortExpression, string nameSearchString)
        {
            if (String.IsNullOrWhiteSpace(sortExpression))
            {
                sortExpression = "Name";
            }
            if (String.IsNullOrWhiteSpace(nameSearchString))
            {
                nameSearchString = "";
            }

            return context.Departments.Include("Person").Include("Courses")
                .OrderBy("it." + sortExpression).Where(d => d.Name.Contains(nameSearchString)).ToList();
        }

        public IEnumerable<Department> GetDepartmentsByAdministrator(Int32 administrator)
        {
            return context.Departments.Include("Person").Where(d => d.Administrator == administrator).ToList();
            //return new ObjectQuery<Department>("SELECT VALUE d FROM Departments as d", context, MergeOption.NoTracking).Include("Person").Where(d => d.Administrator == administrator).ToList();
        }

        public IEnumerable<InstructorName> GetInstructorNames()
        {
            return context.InstructorNames.OrderBy("it.FullName").ToList();
        }

        public void InsertDepartment(Department department)
        {
            try
            {
                department.DepartmentID = GenerateDepartmentID();
                context.Departments.AddObject(department);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                //Include catch blocks for specific exceptions first, 
                //and handle or log the error as appropriate in each. 
                //Include a generic catch block like this one last. 
                throw ex;
            }
        }

        private Int32 GenerateDepartmentID()
        {
            Int32 maxDepartmentID = 0;
            var department = (from d in GetDepartments()
                              orderby d.DepartmentID descending
                              select d).FirstOrDefault();
            if (department != null)
            {
                maxDepartmentID = department.DepartmentID + 1;
            }
            return maxDepartmentID;
        }

        public void UpdateDepartment(Department department, Department origDepartment)
        {
            try
            {
                context.Departments.Attach(origDepartment);
                context.ApplyCurrentValues("Departments", department);
                //context.SaveChanges();
                SaveChanges();
            }
            catch (Exception ex)
            {
                //Include catch blocks for specific exceptions first, 
                //and handle or log the error as appropriate in each. 
                //Include a generic catch block like this one last. 
                throw ex;
            }
        }   

        public void DeleteDepartment(Department department)
        {
            try
            {
                context.Departments.Attach(department);
                context.Departments.DeleteObject(department);
                //context.SaveChanges();
                SaveChanges();
            }
            catch (Exception ex)
            {
                //Include catch blocks for specific exceptions first,
                //and handle or log the error as appropriate in each.
                //Include a generic catch block like this one last.
                throw ex;
            }
        }

        #endregion


        #region OfficeAssignment

        public IEnumerable<OfficeAssignment> GetOfficeAssignments(string sortExpression)
        {
            return new ObjectQuery<OfficeAssignment>("SELECT VALUE o FROM OfficeAssignments AS o", context).Include("Person").OrderBy("it." + sortExpression).ToList();
        }

        public void InsertOfficeAssignment(OfficeAssignment officeAssignment)
        {
            context.OfficeAssignments.AddObject(officeAssignment);
            context.SaveChanges();
        }

        public void DeleteOfficeAssignment(OfficeAssignment officeAssignment)
        {
            context.OfficeAssignments.Attach(officeAssignment);
            context.OfficeAssignments.DeleteObject(officeAssignment);
            context.SaveChanges();
        }

        public void UpdateOfficeAssignment(OfficeAssignment officeAssignment, OfficeAssignment origOfficeAssignment)
        {
            context.OfficeAssignments.Attach(origOfficeAssignment);
            context.ApplyCurrentValues("OfficeAssignments", officeAssignment);
            SaveChanges();
        }

        #endregion

        

    }
}