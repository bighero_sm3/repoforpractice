﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Courses.aspx.cs" Inherits="ContosoUniversity.Courses" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h2>Courses by Department</h2>
<p>
    Select a department:
    <asp:DropDownList ID="DepartmentsDropDownList" runat="server" 
        DataSourceID="DepartmentsEntityDataSource" DataTextField="Name" 
        DataValueField="DepartmentID" AutoPostBack="True">
    </asp:DropDownList>
</p>
    <asp:EntityDataSource ID="DepartmentsEntityDataSource" runat="server" 
        ContextTypeName="ContosoUniversity.DAL.SchoolEntities" 
        EnableFlattening="False" EntitySetName="Departments" 
        Select="it.[DepartmentID], it.[Name]">
    </asp:EntityDataSource>
<p>
    <asp:ValidationSummary ID="CoursesValidationSummary" runat="server" 
        ShowSummary="true" DisplayMode="BulletList" style="color: Red; width: 40em;"  />
    <asp:GridView ID="CoursesGridView" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="CourseID,Title,Credits,DepartmentID"
        DataSourceID="CoursesEntityDataSource">
        <Columns>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True" />
            <asp:BoundField DataField="CourseID" HeaderText="CourseID" ReadOnly="True" SortExpression="CourseID" />
            <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
            <asp:BoundField DataField="Credits" HeaderText="Credits" SortExpression="Credits" />
        </Columns>
    </asp:GridView>
</p>
    <asp:EntityDataSource ID="CoursesEntityDataSource" runat="server" 
        AutoGenerateWhereClause="True" ContextTypeName="ContosoUniversity.DAL.SchoolEntities"
        EnableFlattening="False" EntitySetName="Courses" Where=""
        EnableUpdate="true" EnableDelete="true"
        OnDeleted="CoursesEntityDataSource_Deleted" 
        OnUpdated="CoursesEntityDataSource_Updated">
        <WhereParameters>
            <asp:ControlParameter ControlID="DepartmentsDropDownList" Name="DepartmentID" 
                PropertyName="SelectedValue" Type="Int32" />
        </WhereParameters>
    </asp:EntityDataSource>
<h2>Courses by Name</h2>
<p>
    Enter a course name  
    <asp:TextBox ID="SearchTextBox" runat="server" AutoPostBack="true"></asp:TextBox> 
    &nbsp;<asp:Button ID="SearchButton" runat="server" Text="Search" /> 
</p>
<p>
    <asp:GridView ID="SearchGridView" runat="server" AutoGenerateColumns="False"  
        DataKeyNames="CourseID" DataSourceID="SearchEntityDataSource"  AllowPaging="true"> 
        <Columns> 
            <asp:TemplateField HeaderText="Department"> 
                <ItemTemplate> 
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Department.Name") %>'></asp:Label> 
                </ItemTemplate> 
            </asp:TemplateField> 
            <asp:BoundField DataField="CourseID" HeaderText="ID"/> 
            <asp:BoundField DataField="Title" HeaderText="Title" /> 
            <asp:BoundField DataField="Credits" HeaderText="Credits" /> 
        </Columns> 
    </asp:GridView>
</p>
    <asp:EntityDataSource ID="SearchEntityDataSource" runat="server"
        ContextTypeName="ContosoUniversity.DAL.SchoolEntities" EnableFlattening="False"  
        EntitySetName="Courses"   
        Include="Department" > 
    </asp:EntityDataSource>
    <asp:QueryExtender ID="SearchQueryExtender" runat="server" 
        TargetControlID="SearchEntityDataSource">
        <asp:SearchExpression SearchType="Contains" DataFields="Title">
            <asp:ControlParameter ControlID="SearchTextBox" />
        </asp:SearchExpression>
        <asp:OrderByExpression DataField="Department.Name" Direction="Ascending">
            <asp:ThenBy DataField="Title" Direction="Ascending" />
        </asp:OrderByExpression>
    </asp:QueryExtender>
</asp:Content>
